#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <conio.h>

#define V 30
#define H 70
#define N 500

typedef struct{
    int x,y;
    int ModX, ModY;
    char imagen;
}snk;

typedef struct{
    int x,y;
}frt;

snk snake [N];
frt fruta;

void inicio(int *tam, char campo[V][H]);
void Intro_Campo (char campo[V][H]);
void Intro_Datos(char campo[V][H], int tam);
void loop(char campo[V][H], int tam, int *fresa);
void input(char campo[V][H], int *tam, int *fresa, int *muerto);
void update(char campo[V][H], int tam);
void Intro_Datos2(char campo[V][H],int tam );

int main () {


    int tam, fresa=0, x;
    char campo[V][H];
     printf("               MENU PRINCIPAL\n");
    printf("\n====================SNAKE====================\n");
    printf("\n      ***********CONTROLES***********\n");
    printf("        DIRECCION--------------TECLA\n");
    printf("         ARRIBA                   W\n");
    printf("         ABAJO                    S\n");
    printf("         IZQUIERDA                A\n");
    printf("         DERECHA                  D\n");
    printf("\n_-_-_-_-_-_-_-_-OPCIONES_-_-_-_-_-_-_-_-\n");
    printf("      1:JUGAR                 2:SALIR\n");
    scanf("%d", &x);
    do {
        switch(x)
        {
            case 1:
                inicio(&tam, campo); /* inicia los elementos del juego */
                loop(campo,tam,&fresa);
                break;
            case 2:
                printf("\n\n\n  HASTA LUEGO, GRACIAS POR JUGAR <3\n\n\n\n\n\n");
                break;
        }
        if (x==1)
        {
            printf("=======SNAKE======\n 1:JUGAR\n 2:SALIR\n");
            scanf("%d", &x);
        }
    } while (x!=2);

    system ("pause");
    return 0;
}


void inicio(int *tam, char campo[V][H]){
    int i;
    printf("\nCONTROLES:\n W= ARRIBA \n S= ABAJO \n A= IZQUIERDA \n D= DERECHA \n");
/*inicio de la serpiente*/
    snake[0].x = 35;
    snake[0].y = 15;

/*tama�o de serpiente*/
    *tam = 4;

/*donde salen las frutas*/
    srand(time(NULL));

    fruta.x = rand() % (H - 1);
    fruta.y = rand() % (V - 1);

    while(fruta.x == 0) {
        fruta.x = rand() % (H - 1);
    }
    while(fruta.y == 0) {
        fruta.y = rand() % (V - 1);
    }
    for(i = 0; i< *tam; i++){
/*avances de la serpiente*/
        snake[i].ModX = 1;
        snake[i].ModY = 0;
    }

    Intro_Campo(campo);
    Intro_Datos(campo, *tam);

}

/* campo de juego*/

void Intro_Campo (char campo[V][H]){
    int i,j;
    printf("\nCONTROLES:\n W= ARRIBA \n S= ABAJO \n A= IZQUIERDA \n D= DERECHA \n");
    for (i = 0 ; i < V; i++){
        for (j = 0 ; j < H; j++){
            if (i == 0 || i== V - 1){
                campo[i][j] = '-';
            }
            else if (j == 0 || j== H - 1){
                campo[i][j] = '|';
            }
            else {
                campo[i][j]= ' ';
            }
        }
    }
}
/*datos de matriz tam */
void Intro_Datos(char campo[V][H], int tam){
    int i;

    for(i = 1; i< tam; i++){
        snake[i].x = snake[i - 1].x-1;
        snake[i].y = snake[i - 1].y;

        snake[i].imagen = 'O';
    }
    snake[0].imagen = 'D';
    for(i = 0; i < tam; i++){
        campo[snake[i].y][snake[i].x] = snake[i].imagen;
    }
    campo[fruta.y][fruta.x] = '%';
}




void draw(char campo[V][H]){
    int i, j;

    for (i = 0; i < V; i++){
        for (j = 0; j < H; j++){
            printf("%c",campo[i][j]);
        }
        printf("\n");
    }
}

void loop(char campo[V][H], int tam, int *fresa){
    int muerto;

    muerto = 0;

    do{
        system("cls");
        draw(campo);
        input(campo, &tam, fresa ,&muerto);
        update(campo,tam);
        printf("las frutas comidas fueron= %d \n", *fresa);

    }while (muerto == 0);
}

void input(char campo[V][H], int *tam, int *fresa, int *muerto){
    int i;
    char key;

    /*comprobar muertes*/
    if(snake[0].x == 0 || snake[0].x == H - 1 || snake[0].y == 0 || snake[0].y == V - 1){
        *muerto = 1;
    }

    for(i = 1; i < *tam && *muerto == 0; i++){
        if (snake[0].x == snake[i].x && snake[0].y == snake[i].y){
            *muerto = 1;
        }
    }
/*comprobar comer frutas :v */
    if(snake[0].x == fruta.x && snake[0].y == fruta.y){
        *tam += 1;
        *fresa += 1;
        snake[*tam - 1].imagen = 'O';

/*nueva coordenada de la fruta*/
        fruta.x = rand() % (H - 1);
        fruta.y = rand() % (V - 1);

        while(fruta.x == 0) {
            fruta.x = rand() % (H - 1);
        }
        while(fruta.y == 0) {
            fruta.y = rand() % (V - 1);
        }
    }

    if(*muerto == 0){
        if(kbhit() == 1){
            key = getch();

            if (key == 's' && snake[0].ModY != -1 ){
                snake[0].ModX= 0;
                snake[0].ModY =1;
            }
            if (key == 'w' && snake[0].ModY != 1){
                snake[0].ModX= 0;
                snake[0].ModY =-1;
            }
            if (key == 'a' && snake[0].ModX != 1){
                snake[0].ModX = -1;
                snake[0].ModY = 0;
            }
            if (key == 'd' && snake[0].ModX != -1){
                snake[0].ModX = 1;
                snake[0].ModY = 0;
            }
        }
    }
}

void update(char campo[V][H], int tam){
/*borra datos de la matriz*/
    Intro_Campo(campo);

/*nuevos datos*/
    Intro_Datos2(campo, tam);
}

void Intro_Datos2(char campo[V][H],int tam ){
    int i;
/*hacer que la serpiente se mueva*/

    for (i = tam - 1; i > 0 ; i--){
        snake[i].x = snake[i-1].x;
        snake[i].y = snake[i-1].y;
    }
    snake[0].x += snake[0].ModX;
    snake[0].y += snake[0].ModY;

    for(i = 0; i<tam; i++){
        campo[snake[i].y][snake[i].x] = snake[i].imagen;

    }

    campo[fruta.y][fruta.x]= '%';
}
